const puppeteer = require('puppeteer');
const cheerio  = require('cheerio');

const inputUser = 'body > div.row > div > div > div:nth-child(2) > div:nth-child(3) > div > div > form > div.sign-up-fields.marbot24 > input[type=text]';
const userEnter = 'body > div.row > div > div > div:nth-child(2) > div:nth-child(3) > div > div > form > div.martop12 > button';

const inputPassword = 'body > div.row > div > div > div:nth-child(2) > div:nth-child(3) > div > div > form > div.sign-up-fields > input[type=password]:nth-child(2)';
const passwordEnter = 'body > div.row > div > div > div:nth-child(2) > div:nth-child(3) > div > div > form > div.martop12 > button';

const dataTablesel = 'body > div:nth-child(3) > div.col-md-9 > div > div:nth-child(2) > div:nth-child(4) > div > div > div.dashboard-table-body > table > tbody';

const BASE_URL = `https://skystreamx.leaddyno.com/affiliate/`;
const CONTINUE_WORK = true;
const END_WORK = false;


(async () => {
	
	const user='doron@yellowheadinc.com';
	const password='11free22!@#';
	const {browser, page, isLogin} = await CheckLogin(BASE_URL, user, password);

	if(isLogin) RipDataFromPage(browser, page);
	else return false;

})();


async function CheckLogin(url, user, password){
	const navigator = await puppeteer.launch({
		headless: true,
		ignoreHTTPSErrors: true,
		devtools: false,
		timeout: 10000,
		args: ['--no-sandbox']
	});
//	const user='doron@yellowheadinc.com';
//	const password='11free22!@#';
	const page = await navigator.newPage();
	await page.goto(url);
	
	// user Enter
	console.log(`Enter LOGIN`);
	await page.waitFor(1000);
	await page.type(inputUser, user);
	await page.click(userEnter);

	// password enter
	console.log(`Enter PASSWORD`);
	await page.waitFor(1000); // wait loading password form
	await page.type(inputPassword, password);
	await page.click(passwordEnter);

	// check for correct login
	await page.waitFor(1000); //await page.waitForNavigation(1000);
	const content = await page.content();
	
	const alert = isCorrectLogin(content) ;
	if(alert >= 0) {
		await navigator.close();

		console.log(`Login or Password is INCORRECT`);
		return {isLogin: false};
	}

	console.log(`Login is CORRECT`);
	
	return {browser : navigator, page : page, isLogin : true};
};

const isCorrectLogin = (html) => {
	const alert = html.indexOf('Invalid Password');
	return alert;
}

async function RipDataFromPage(browser, page) {
	console.log(`RIP PAGES DATA`);
	const homeURL = `${BASE_URL}/commission_details`;
	// goto working page
	await page.goto(homeURL); //получили страничку в виде готового HTML - далее парсинг данных. в случае замены кукловода - вот до этой точки он, а потом отдеьный метод

	const html = await page.content();

	const totalPages = GetTotalPages(html);
	
	console.log('Start Get Table Data');
	// get data from all pages
	const table = [];
	for(let i = 1; i <= totalPages; i++) {
		console.log(`Page N ${i} wait for navigation on ` + homeURL + `?page=${i}`);
		await page.goto(homeURL + `?page=${i}`);

		//get table from regx
		console.log(`Page N ${i} wait content`);

		const $ = cheerio.load(await page.content());
		const trList = [];
		$('tr', dataTablesel).each(function(i, e){
			trList[i] = $(this).html();
		}); //[1].innerHTML;
//		let tbody = parse(dataTable).querySelector('tbody');
//		let trList = parse(tbody.innerHTML).querySelectorAll('tr');

		console.log(`Page N ${i} get table data`);
		if(PushDataInTable(table, trList) === END_WORK) {
			console.log(`Date limit reached on page ${i}`);
			break;
		}
	}
	
	console.log(`All work complited`);
	await browser.close();

	return table;
}

// return true if if the time limit is reached
// return false if all OK
// table - where did write
// data - from get information
const PushDataInTable = (table, data) => {
	let continue_work = true;

	data.forEach((value, i) => {
		const tdList = value
						.replace(new RegExp(`"`, `g`), ``)
						.replace(new RegExp(`</td>`, `g`), ``)
						.split(`<td>`);

		const date_now = new Date();
		const date_data = new Date(tdList[1]);

		if((date_now - date_data) < (90 * 24 * 3600 * 1000)) {
			table.push({
				created		: tdList[1],
				for			: tdList[2],
				customer	: tdList[3],
				due_date	: tdList[4],
				status		: tdList[5],
				amount		: tdList[6],
			});
		} else {
			continue_work = false
		}
		
	});
	

	return continue_work;
};

// a checking date for limit in days
// value - text of date
// limit - integer days
const DateChecker = (value, limit) => {

	let dateForCheck = new Date(value);

	let dateNow = new Date();

	let days = (dateNow - dateForCheck) / 1000 / 3600 / 24;

	return ((days < limit) ? true : false);	
};

// page is HTML text
const GetTotalPages = (page) => {
	// парсним входную html
	const $ = cheerio.load(page);

//	console.log(html);
	// try get pages list
	const pagesList = $('.pagination').text().split(' ');/*html.querySelector('.pagination');/*await page.$eval(dataPages, (data) => {
		return data.innerHTML;
	});*/
	console.log(pagesList);

	const lastNumb = pagesList
						.map((item, i) => {
							if(item.toLowerCase() === 'next') 
								return (i - 1);
						})
						.filter((item) => {
							if(parseInt(item) > 0) 
								return item;
						});

	console.log('----------------------------------------------------------------');
	console.log(`Total pages count: ${pagesList[lastNumb]}`);

	return pagesList[lastNumb];
};
